//
//  HolidayCellViewModelTests.swift
//  BAHolidayCalendarTests
//
//  Created by nic on 02/06/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import XCTest
@testable import BAHolidayCalendar

class HolidayCellViewModelTests: XCTestCase {

    var holidayViewModel: HolidayCellViewModel!
    
    override func setUp() {
        let jsonHoliday = """
        {
            "meta": {
           "code": 200
            },

            "response": {
            "holidays": [
            {
                "name": "New Year's Day",
                "description": "New Year's Day is the first day of the Gregorian calendar, which is                     widely used in many countries such as the USA.",
                "date": {
                "iso": "2019-01-01",
                "datetime": {
                      "year": 2019,
                      "month": 1,
                      "day": 1
                  }
                },
                "type": [
                 "National holiday"
                ],
                "locations": "All",
                "states": "All"
            }
            ]
          }
        }
        """.data(using: .utf8)
        let decoder = JSONDecoder()
        
        do {
            let holidayService = try decoder.decode(HolidayStoreService.self, from: jsonHoliday!)
            let holiday = holidayService.holidays[0]
            holidayViewModel = HolidayCellViewModel(with: holiday)
            
        } catch {
            print(error)
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDateFormatted() {
        
        XCTAssertEqual(holidayViewModel.dateFormatted, "01 Jan")
    }

    func testIndexOfMonth() {
        XCTAssertEqual(holidayViewModel.indexOfMonth, 0)
    }
}
