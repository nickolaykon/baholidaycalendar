//
//  HolydayModel.swift
//  BAHolydayCalendar
//
//  Created by nic on 22/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//
//  "meta": {
//        "code": 200
//  },
//
//  "response": {
//        "holidays": [
//          {
//              "name": "New Year's Day",
//              "description": "New Year's Day is the first day of the Gregorian calendar, which is                     widely used in many countries such as the USA.",
//              "date": {
//                 "iso": "2019-01-01",
//                 "datetime": {
//                      "year": 2019,
//                      "month": 1,
//                      "day": 1
//                  }
//              },
//              "type": [
//                 "National holiday"
//              ],
//              "locations": "All",
//              "states": "All"
//          }
//      ]
//  }

import Foundation

enum HolidayError: Error {
    case dataNotAvailable
    case canNotProcessData
    case invalidURL
}


struct Holiday: Decodable {
    var name: String
    var description: String?
    var date: Date
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case description = "description"
        case date = "date"
        case iso = "iso"
    }
    
    init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
         
            name = try container.decode(String.self, forKey: .name)
            description = try container.decode(String?.self, forKey: .description)
            let dateContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .date)
        
            var iso = try dateContainer.decode(String.self, forKey: .iso)
        
            if iso.count > 10 {
                iso = String(iso.prefix(10))
            }
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            date = dateFormatter.date(from: iso)!
        
   //     print(name + " : date : " + dateFormatter.string(from: date) + " : iso: " + iso)
        
    }
  
}

struct HolidayStoreService: Decodable {
    private var response: HolidayStore
    var holidays: [Holiday] {
        return response.holidays
    }
    struct HolidayStore: Decodable {
        var holidays: [Holiday]

    }
    
}





