//
//  CountryModel.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit

struct CountryModel: Decodable {
    var name: String
    var code: String
//    var flag: UIImage?
    
    enum CodingKeys: String, CodingKey {
        case name
        case code
     //   case flag
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decode(String.self, forKey: .name)
        code = try values.decode(String.self, forKey: .code)
//
//        flag = UIImage(named: code)
    }
}
