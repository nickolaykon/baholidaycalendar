//
//  CountryDataStore.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation

struct CountryDataStore {
    
    
    //MARK: - private vars
//    private var countryModels: [CountryModel]!
    
    static func getCountryList() -> [CountryModel] {
        
        let decoder = JSONDecoder()
        do {
          let countries = try decoder.decode([CountryModel].self, from: countryListJson!)
            return countries
        } catch {
            print(error)
        }
        return []
    }
}
