//
//  UITableViewCell.swift
//  BAHolydayCalendar
//
//  Created by nic on 22/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var identifier: String {
        return String(describing: self)
    }
}
