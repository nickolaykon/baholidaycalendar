//
//  HolidayRequestSimulatedData.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation

struct HolidayMockData {
    
    static func getMockHolidays() -> [Holiday] {
        let json = #"""
{
    "meta": {
        "code": 200
    },
    "response": {
        "holidays": [
            {
                "name": "New Year's Day",
                "description": "New Year's Day is the first day of the Gregorian calendar, which is widely used in many countries such as the USA.",
                "date": {
                    "iso": "2019-01-01",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 1
                    }
                },
                "type": [
                    "National holiday"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "World Braille Day",
                "description": "World Braille Day celebrates the life and achievements of Louis Braille, who invented the braille code for the visually impaired.",
                "date": {
                    "iso": "2019-01-04",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 4
                    }
                },
                "type": [
                    "Worldwide observance"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "Epiphany",
                "description": "Many people in the United States annually observe Epiphany, or Three Kingsâ€™ Day, on January 6. It is a Christian observance and a public holiday in the US Virgin Islands.",
                "date": {
                    "iso": "2019-01-06",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 6
                    }
                },
                "type": [
                    "Christian"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "Orthodox Christmas Day",
                "description": "Many Orthodox Christian churches in countries such as the United States observe Christmas Day on or near January 7 in the Gregorian calendar.",
                "date": {
                    "iso": "2019-01-07",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 7
                    }
                },
                "type": [
                    "Orthodox"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "International Programmers' Day",
                "description": "Many people celebrate International Programmersâ€™ Day on January 7, while others observe this event on the 256th day of the year, usually on September 13 or on September 12 in leap years.",
                "date": {
                    "iso": "2019-01-07",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 7
                    }
                },
                "type": [
                    "Worldwide observance"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "Stephen Foster Memorial Day",
                "description": "Stephen Foster Memorial Day is annually observed in the United States on January 13. The day remembers the achievements of Stephen Foster, who is known as the â€œfather of American musicâ€.",
                "date": {
                    "iso": "2019-01-13",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 13
                    }
                },
                "type": [
                    "Observance"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "Orthodox New Year",
                "description": "Many Orthodox Christian churches in countries such as the United States celebrate New Yearâ€™s Day on January 14 in the Gregorian calendar.",
                "date": {
                    "iso": "2019-01-14",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 14
                    }
                },
                "type": [
                    "Orthodox"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "Lee-Jackson Day",
                "description": "Lee-Jackson Day is an annual state holiday in Virginia, the United States, on the Friday before Martin Luther King Day in January.",
                "date": {
                    "iso": "2019-01-18",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 18
                    }
                },
                "type": [
                    "Local holiday"
                ],
                "locations": "VA",
                "states": [
                    {
                        "id": 52,
                        "abbrev": "VA",
                        "name": "Virginia",
                        "exception": null,
                        "iso": "us-va"
                    }
                ]
            },
            {
                "name": "Robert E. Lee's Birthday",
                "description": "Robert E. Leeâ€™s birthday is an annual official state holiday that is shared with Martin Luther Kingâ€™s birthday in some parts of the US.",
                "date": {
                    "iso": "2019-01-19",
                    "datetime": {
                        "year": 2019,
                        "month": 1,
                        "day": 19
                    }
                },
                "type": [
                    "Local holiday"
                ],
                "locations": "FL",
                "states": [
                    {
                        "id": 12,
                        "abbrev": "FL",
                        "name": "Florida",
                        "exception": null,
                        "iso": "us-fl"
                    }
                ]
            },
            {
                "name": "September Equinox",
                "description": null,
                "date": {
                    "iso": "2019-09-23T03:50:13-04:00",
                    "datetime": {
                        "year": 2019,
                        "month": 9,
                        "day": 23,
                        "hour": 3,
                        "minute": 50,
                        "second": 13
                    },
                    "timezone": {
                        "offset": "-04:00",
                        "zoneabb": "EDT",
                        "zoneoffset": -18000,
                        "zonedst": 3600,
                        "zonetotaloffset": -14400
                    }
                },
                "type": [
                    "Season"
                ],
                "locations": "All",
                "states": "All"
            },
            {
                "name": "World Tourism Day",
                "description": "The United Nationsâ€™ (UN) World Tourism Day is annually held on September 27 to raise awareness on the benefits of tourism.",
                "date": {
                    "iso": "2019-09-27",
                    "datetime": {
                        "year": 2019,
                        "month": 9,
                        "day": 27
                    }
                },
                "type": [
                    "United Nations observance"
                ],
                "locations": "All",
                "states": "All"
            }
        ]
    }
}
"""#.data(using: .utf8)
        
            let decoder = JSONDecoder()
        
        do {
            let result = try decoder.decode(HolidayStoreService.self, from: json!)
            return result.holidays
        } catch {
            print(error)
        }
        return []
    }
}
