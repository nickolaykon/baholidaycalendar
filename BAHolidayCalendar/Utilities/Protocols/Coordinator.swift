//
//  Coordinator.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    var navController: UINavigationController {get set}
    func start()
}
