//
//  Storyboarded.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit
protocol Storyboarded: AnyObject {
    static func instantiateAsInitial() -> Self
}

extension Storyboarded where Self: UIViewController {
    static func instantiateAsInitial() -> Self {
        let storyboard = UIStoryboard(name: String(describing: self), bundle: Bundle.main)
        return storyboard.instantiateInitialViewController() as! Self
    }
}
