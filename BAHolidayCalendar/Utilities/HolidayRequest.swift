//
//  HolydayRequest.swift
//  BAHolydayCalendar
//
//  Created by nic on 22/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation

struct HolidayRequest {
    private static let api_key = "3db18039ee51e3a78429056784bcb2120a3c4289"
    private static let scheme = "https"
    private static let host = "calendarific.com"
    private static let path = "/api/v2/holidays"
    private static let year = "2019"
    
    //"https://calendarific.com/api/v2/holidays?&api_key=3db18039ee51e3a78429056784bcb2120a3c4289&country=US&year=2019"
    
    static func getHolidays(for countryCode: String, completion: @escaping(Result<[Holiday], HolidayError>) -> Void) {
        
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        urlComponents.queryItems = [
            URLQueryItem(name: "api_key", value: api_key),
            URLQueryItem(name: "country", value: countryCode),
            URLQueryItem(name: "year", value: year)
        ]
        
        guard let url = urlComponents.url else {
            completion(.failure(.invalidURL))
            return
        }
        
        let dataTask = URLSession.shared.dataTask(with: url) { (data, _ , _) in
            
            guard let jsonData = data else {
                completion(.failure(.dataNotAvailable))
                return
            }
            
            do {
                let decoder = JSONDecoder()
                let  result = try decoder.decode(HolidayStoreService.self, from: jsonData)
                completion(.success(result.holidays))
            } catch {
                completion(.failure(.canNotProcessData))
            }
        }
        dataTask.resume()
    }
    
    
    
}
