//
//  CountriesListViewController.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import UIKit

class CountriesListViewController: UIViewController, Storyboarded {

    weak var coordinator: MainCoordinator!
    @IBOutlet weak var contentView: UIView!
   @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Private vars
    private var countries = [CountryCellViewModel]()
    private var filteredCountries = [CountryCellViewModel]()
    private var isFiltering = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.addRoundedCorners()
        
        fetchData()
        setupTableView()
        setupSearch()

    }
}

//MARK: - User Interactions
extension CountriesListViewController {
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Private functions
private extension CountriesListViewController {
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.addRoundedCorners()
    }
    
    func fetchData() {
        countries = CountryDataStore.getCountryList().map({ CountryCellViewModel(with: $0) })
    }
    
    func setupSearch() {
        searchBar.delegate = self
    }
    
    func filterCountries(by substring: String) -> [CountryCellViewModel] {
        
        let result = countries.filter { (country) -> Bool in
            return country.title.lowercased().contains(substring.lowercased())
        }
        return result
    }
}

//MARK: - UITableViewDelegate
extension CountriesListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCountry = isFiltering ? filteredCountries[indexPath.row] : countries[indexPath.row]
        
        coordinator.selectCountry(selectedCountry.title, withCode: selectedCountry.code)
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - UITableViewDataSource
extension CountriesListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFiltering ? filteredCountries.count : countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: CountryCell.identifier, for: indexPath) as! CountryCell

        let country = isFiltering ? filteredCountries[indexPath.row] : countries[indexPath.row]

        cell.configure(with: country)
        
        return cell
    }
}

//MARK: - UISearchBarDelegate
extension CountriesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if (searchText != "" ) {
            isFiltering = true
            filteredCountries = filterCountries(by: searchText)
        } else {
            isFiltering = false
        }
        tableView.reloadData()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
        isFiltering = false
        tableView.reloadData()
    }
    
}
