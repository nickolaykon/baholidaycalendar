//
//  CountryCell.swift
//  BAHolidayCalendar
//
//  Created by nic on 27/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with country: CountryCellViewModel) {
        imageView?.image = country.icon
        textLabel?.text = country.title
    }
}
