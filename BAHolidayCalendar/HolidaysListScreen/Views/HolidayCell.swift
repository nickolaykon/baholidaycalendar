//
//  HolydayCell.swift
//  BAHolydayCalendar
//
//  Created by nic on 22/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import UIKit

class HolidayCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(with holiday: HolidayCellViewModel) {
        
        titleLabel.text = holiday.title
        dateLabel.text = holiday.dateFormatted
    }

}
