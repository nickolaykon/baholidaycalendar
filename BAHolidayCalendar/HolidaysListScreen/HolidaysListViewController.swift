//
//  HolydaysListViewController.swift
//  BAHolydayCalendar
//
//  Created by nic on 22/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import UIKit



class HolidaysListViewController: UIViewController, Storyboarded {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var statusBarLabel: UILabel!
    
    @IBOutlet weak var shadeView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var tableFooterView: UIView!
    @IBOutlet weak var tableFooterViewLabel: UILabel!
    
    weak var coordinator: MainCoordinator!
    var viewModel = HolidaysListViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        activityIndicator.startAnimating()
        
        setupTableView()

        setupSearchNavigator()
        
        bindToViewModel()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        backgroundImageView.removeVisualEffect()
        backgroundImageView.addDarkBlurEffect()
    }
    
    //MARK: - private vars
    private let searchController = UISearchController(searchResultsController: nil)
    private let defaultCountryCode = "US"
    private var sectionTitles = [String]()

}
//MARK: - UI Interactions
extension HolidaysListViewController {
    
    @IBAction func showCountryListTapped(_ sender: UIButton) {
        coordinator.showCountryList()
    }
}

//MARK: - UISearchResultsUpdating
extension HolidaysListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {

    }
}
//MARK: - UISearchBarDelegate
extension HolidaysListViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.state = .endFilteringByTitle
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        viewModel.state = .beginFilteringByTitle(searchText)
    }
    
}

//MARK: - API
extension HolidaysListViewController {
    
    func showHolidaysForCountry(_ title: String, withCode code: String) {

        viewModel.fetchDataForCountry(title, withCode: code)
    }
}



//MARK: - Private functions
private extension HolidaysListViewController {
    
    func setupSearchNavigator() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search holiday"
        searchController.searchBar.delegate = self
        definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
    }
    
    func bindToViewModel() {
        viewModel.title.bind { [unowned self] (string) in
              DispatchQueue.main.async {
                self.title = string
            }
        }
        
        viewModel.isNeededActivityIndicator.bind { [unowned self] (isNeeded)in
            DispatchQueue.main.async {
                isNeeded == true ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
            }
        }

        viewModel.textInFooterView.bind { [unowned self] (string )in
            DispatchQueue.main.async {
                if string.isEmpty {
                    self.tableView.tableFooterView = nil
                } else {
                    self.tableFooterViewLabel.text = string
                    self.tableView.tableFooterView = self.tableFooterView
                }
            }
        }
        viewModel.reloadView = { [unowned self] in
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }

    }
    
    func setupTableView() {
        
        tableView.addRoundedCorners()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 80


        sectionTitles.append("January")
        sectionTitles.append("February")
        sectionTitles.append("March")
        sectionTitles.append("April")
        sectionTitles.append("May")
        sectionTitles.append("June")
        sectionTitles.append("July")
        sectionTitles.append("August")
        sectionTitles.append("September")
        sectionTitles.append("October")
        sectionTitles.append("November")
        sectionTitles.append("December")

    }
    
}

//MARK: - UITableViewDelegate
extension HolidaysListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if case HolidaysListViewModel.HolidaysListState.populated(_) = viewModel.state {
            return sectionTitles[section]
        }

        if case HolidaysListViewModel.HolidaysListState.filteredByTitle(let holidays) = viewModel.state {
            return "Matches \(holidays.count) of \(viewModel.totalHolidayCount)"
        }

        if case HolidaysListViewModel.HolidaysListState.empty = viewModel.state {
            return ""
        }
        
        return ""

    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {

        guard let headerView = view as? UITableViewHeaderFooterView else { return }
        headerView.backgroundView?.backgroundColor = Theme.Colors.backgroundDark
        headerView.textLabel?.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
    }
    
    
}

//MARK: - UITableViewDataSource
extension HolidaysListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if case HolidaysListViewModel.HolidaysListState.filteredByTitle(_) = viewModel.state {
            return 1
        } else {
            return viewModel.state.currentHolidays.count
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if case let HolidaysListViewModel.HolidaysListState.filteredByTitle(holidays) = viewModel.state {
            return holidays.count
        } else {
            return viewModel.state.currentHolidays[section].count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: HolidayCell.identifier) as? HolidayCell else { return UITableViewCell()}

        if case let HolidaysListViewModel.HolidaysListState.filteredByTitle(holidays) = viewModel.state {
            cell.configure(with: holidays[indexPath.row])
        } else {
            cell.configure(with: viewModel.state.currentHolidays[indexPath.section][indexPath.row])
        }
        return cell
    }
    
}

