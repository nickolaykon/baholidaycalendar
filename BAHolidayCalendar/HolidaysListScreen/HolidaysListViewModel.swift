//
//  HolidaysListViewModel.swift
//  BAHolidayCalendar
//
//  Created by nic on 30/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation

class HolidaysListViewModel {
    
    enum HolidaysListState {
        case loading
        case error(Error)
        case beginFilteringByTitle(String)
        case filteredByTitle([HolidayCellViewModel])
        case endFilteringByTitle
        case empty
        case populated([[HolidayCellViewModel]])
        
        var currentHolidays: [[HolidayCellViewModel]] {
            switch self {
            case .populated(let holidays):
                return holidays
            
            default:
                return []
            }
        }
    }
    
    var statusBarText: String = "" {
        didSet {
            updateStatusBar?(statusBarText)
        }
    }
    
    var state: HolidaysListState {
        didSet {
            textInFooterView.value = ""
            title.value = currentCountryName

            switch state {
            case .loading:

                isNeededActivityIndicator.value = true
            case .populated(let holidays):

                isNeededActivityIndicator.value = false
                
                reloadView?()
                let counter = holidays
                title.value = "\(currentCountryName) total: \(counter.flatMap({ $0 }).count)"
                
            case .beginFilteringByTitle(let searchString):
                if searchString != "" {
                    let result = filterHolidays(by: searchString)
                    state = .filteredByTitle(result)
                } else {
                    state = .populated(initialHolidayCollection)
                }
                
                reloadView?()
                
            case .endFilteringByTitle:
                state = .populated(initialHolidayCollection)
                reloadView?()
            case .empty:
                textInFooterView.value = "No results. Try anything else."
            case .error(let error):
                isNeededActivityIndicator.value = false
                textInFooterView.value = "Oops. Something went wrong due to: \(error.localizedDescription). Try again later"
                reloadView?()
                print(error.localizedDescription)
            default:
                print("default")
            }
        }
    }
    
    //MARK: - Private vars
    private var initialHolidayCollection = [[HolidayCellViewModel]]()
    var totalHolidayCount: Int {
        let result = initialHolidayCollection
        return result.flatMap( {$0 }).count
    }
    private var currentCountryName = "United States"
    private var currentCountryCode = "US"
    
    
    var title: Box<String?> = Box(nil)
    var isNeededActivityIndicator: Box<Bool> = Box(false)
    var textInFooterView: Box<String> = Box("")
    
    //MARK: - Closures
    var reloadView: (() -> Void)?
    var updateStatusBar: ((String) -> Void)?
  
    //MARK: - Init

    init() {
        
        state = .loading
        fetchMockData()
     //   fetchDataForCountry("USA", withCode: "US")
    }
}



//MARK: - API
extension HolidaysListViewModel {
    
    func fetchDataForCountry(_ countryName: String, withCode countryCode: String) {
        state = .loading
        
        currentCountryCode = countryCode
        currentCountryName = countryName
        HolidayRequest.getHolidays(for: countryCode) { (result) in
            switch result {
            case .success(let holidays):
                var result = [[HolidayCellViewModel]]()
                for _ in 0 ..< 12 {
                    result.append([])
                }
                
                for holiday in holidays {
                    let viewModel = HolidayCellViewModel(with: holiday)
                    result[viewModel.indexOfMonth].append(viewModel)
                }
                self.initialHolidayCollection = result
                
                self.state = .populated(result)
                
            case .failure(let error):
                self.state = .error(error)
            }
        }
    }
}

//MARK: - Private functions
private extension HolidaysListViewModel {
     func filterHolidays(by searchString: String) -> [HolidayCellViewModel] {
        var result = initialHolidayCollection.flatMap( { $0 } )
        

        result = result.filter({ (holidayModel) -> Bool in
             return holidayModel.title.lowercased().contains(searchString.lowercased())
        })
            
        if result.count == 0 {
            state = .empty
        }
        
        return result
    }
    
    
    func fetchMockData() {
        state = .loading
        
        let deadline = DispatchTime.now() + .seconds(10)
        DispatchQueue.main.asyncAfter(deadline: deadline) { [weak self]  in
            
            let holidays = HolidayMockData.getMockHolidays()
            
            var result = [[HolidayCellViewModel]]()
            
            for _ in 0 ..< 12 {
                result.append([])
            }
            
            for holiday in holidays {
                
                let viewModel = HolidayCellViewModel(with: holiday)
                result[viewModel.indexOfMonth].append(viewModel)
            }
            
            self?.title.value = "Total holidays: \(result.count)"
            self?.initialHolidayCollection = result
            self?.state = .populated(result)

        }
    }

}
