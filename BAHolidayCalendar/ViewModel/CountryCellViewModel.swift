//
//  CountryViewModel.swift
//  BAHolidayCalendar
//
//  Created by nic on 27/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit

struct CountryCellViewModel {
    let title: String
    let code: String
    let icon: UIImage?
    
    init(with country: CountryModel) {
        title = country.name
        code = country.code
        icon = UIImage(named: country.code)
    }
}
