//
//  HolidayCellViewModel.swift
//  BAHolidayCalendar
//
//  Created by nic on 30/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation

struct HolidayCellViewModel {
    let title: String
    let description: String?
    let date: Date
    
    var dateFormatted: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM"
        return dateFormatter.string(from: date)
    }
/// Returns index of month, starting from 0
    var indexOfMonth: Int {
        let calendar = Calendar.current
        let result = (calendar.component(.month, from: date) - 1)
        
        return result
    }
    
    init(with holiday: Holiday) {
        title = holiday.name
        description = holiday.description
        date = holiday.date
    }
}
