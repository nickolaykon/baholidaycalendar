//
//  MainCoordinator.swift
//  BAHolidayCalendar
//
//  Created by nic on 25/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: Coordinator {
    var navController: UINavigationController
    
    init(with navController: UINavigationController) {
        self.navController = navController
    }
    
    
    func start() {
        let vc = HolidaysListViewController.instantiateAsInitial()
        vc.coordinator = self
        navController.pushViewController(vc, animated: false)
    }
    
    func showCountryList() {
     //   let vc = CountriesListViewController.instantiateAsInitial()
        
        let storyboard = UIStoryboard(name: String(describing: CountriesListViewController.self), bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: CountriesListViewController.self)) as! CountriesListViewController
        vc.coordinator = self
        navController.present(vc, animated: true, completion: nil)
    }
    
    func selectCountry(_ country: String, withCode countryCode: String ) {
      //  print("country code is: " + code)
        guard let vc  = navController.topViewController as? HolidaysListViewController else {return}
        
        vc.showHolidaysForCountry(country, withCode: countryCode)
        
    }
}
