//
//  Theme.swift
//  BAHolidayCalendar
//
//  Created by nic on 29/05/2019.
//  Copyright © 2019 nic. All rights reserved.
//

import Foundation
import UIKit

struct Theme {
    struct Colors {
        static let main = UIColor(named: "main")
        static let accent = UIColor(named: "accent")
        static let backgroundDark = UIColor(named: "background")
    }
}
